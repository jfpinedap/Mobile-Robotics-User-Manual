Mobile Robotics USER MANUAL
===========================

This user manual shows you how operate the Self Driving Vehicles SDVs from LabFabEx laboratory at the Universidad Nacional de Colombia.

Prerequisites:
--------------

 - You need to install:

   1. Ubuntu [16.04](https://www.ubuntu.com/download/desktop) or latest.
   
   2. ROS [kinetic](http://wiki.ros.org/kinetic/Installation/Ubuntu) and create a ROS [Workspace](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment).

   3. Vim, Nano or other Terminal-based text editor.
      
    On terminal...

      ```bash
      sudo apt update
      sudo apt install vim -y
      ```

   4. Create a SSH key and install SSH client and server.
      
    On terminal...

      ```bash
      ssh-keygen -t rsa -C "your.email@example.com" -b 4096
      ssh-add ~/.ssh/id_rsa

      sudo apt update
      sudo apt install openssh-client openssh-server -y 
      ```

   5. Git (optional).

Table of contents
=================

<!--ts-->
   * [Mobile Robotics USER MANUAL](#mobile-robotics-user-manual)
      * [Prerequisites](#prerequisites)
   * [Table of contents](#table-of-contents)
   * [Get Started](#get-started)
   * [Network SDV connection](#network-sdv-connection)
      * [SSH connection](#ssh-connection)
      * [Modify the vehicle's _.bashrc_](#modify-the-vehicles-bashrc)
      * [Modify the _.bashrc_ of your personal computer](#modify-the-bashrc-of-your-personal-computer)
   * [Launch the applications](#launch-ros-navigation-stack)
   * [Navigate](#navigate)
   * [Turn Off vehicles](#turn-off-vehicles)

<!--te-->


Get Started
===========

1.**Authorization:**
   To operate any SDV (Self Driving Vehicle) you must have explicit permission from the director of LabFabEx, Ernesto Córdoba Nieto.

2.**Previous preparations:**
   With the explicit authorization you can schedule an appointment with the LabFabEx technical staff who prepares the vehicle for operation (charge batteries and install them with their respective buzzers).

3.**Check operational state of the SDV:**
    For correct buzzers connection the black wire must be connected with the negative symbol in the buzzer.

| ![buzzerC.png](img/buzzerC.png) | 
|:--:|
| *Buzzer correctly connected and battery with all cells charged* |

| ![buzzerI.png](img/buzzerI.png) |
|:--:|
| *Buzzer correctly connected but the second battery cell is __discharged__*|

   :red_circle: :red_circle: :red_circle: `Before operate any vehicle PLEASE CHECK if the buzzers are correctly installed.` :red_circle: :red_circle: :red_circle:

   For more information about use and care Lipo Batteries visit [http://learningrc.com](http://learningrc.com/lipo-battery/)

4.**Turn On the SDV:**
   Turn on PC and Laser switches placed in the back of the SDV and then press Intel® NUC power-On button. Later turn on the Drivers Motors switch. It MUST be in this order.

![switches.png](img/switches.png)

Network SDV connection
==============

SSH connection
--------------

The connection is established by SSH and you need a computer with all [prerequisites](#prerequisites) installed.

The vehicles operate on the `LabFabEx` network, therefore is necesary to generate that connection. For the password please contact the LabFabEx technical staff.

This SSH commnad is used to connect to a remote system:

```bash
ssh remote_username@remote_host_IP
```

Where *remote_username* corresponds to sdvun1, sdvun2 or sdvun3 and their remote_hosts_IP are related in the following table.

| **SDV** | __remote_username__ | **remote_host_IP** |
|:---:|:---------------:|:-----------:|
|SDV I| sdvun1 | 192.168.1.11 |
|SDV II| sdvun2 | 192.168.1.12 |
|SDV III| sdvun3 | 192.168.1.13 |


With this command you can open a remote host terminal from the vehicle on the computer where command has been launched. In order to have access it is required typing the password which is the same remote_username.

| ![alt text4](img/prompt.png) |
|:--:|
| *Terminal example connecting via SSH from `esteban` computer to `agvun2` computer: You can see the change to propmt.* |

- **Change .bashrc:** 
In order to establish communication with the vehicle's `ros-master`, it is necessary to modify the `.bashrc` from both computers (vehicle's and user's - your computer).

Modify the vehicle's .bashrc
----------------------------

The vehicles' .bashrc are generaly update and configure to work on the LabFabEx laboratory wifi network `LabFabEx`. Therefore, it is **NOT NECESSARY** to add these lines, however it is highly recommended to verify if the .bashrc file contains these statements that indicate the addresses for `ROS_MASTER_URI`, `ROS_IP` and `ROS_NAME`. On the other hand if you are working on another network it is necessary to change the addresses according to those assigned by the router. 

You can do it using Vim:

   On the prompt `sdvunx@sdvunx-desktop:~$ ` that you get executing the `ssh sdvunx@192.168.1.xx` command, type as follow:

   ```bash
   vim ~/.bashrc 
   ```

   This command open the file .bashrc of the vehicle using Vim. At the end of file you must add the following lines:

   ```bash

   #AGV UN 2
   export ROS_MASTER_URI='http://192.168.1.12:11311'
   export ROS_IP='192.168.1.12'
   export ROS_NAME='192.168.1.12'

   source /opt/ros/kinetic/setup.bash
   source $HOME/catkin_ws/devel/setup.bash
   ```
   
   Then update terminal:
   
   ```bash
   bash 
   ```
Modify the .bashrc of your personal computer
--------------------------------------------

You can do it using Vim:

   On your own prompt, for example `esteban@esteban-X555LN:~$ `, type:

   ```bash
   vim ~/.bashrc 
   ```

   This command open the file .bashrc of your personal computer using Vim, in it you must add the following lines at the end of the file:

   ```bash

   #YOUR PERSONAL COMPUTER
   export ROS_MASTER_URI='http://192.168.1.12:11311'
   export ROS_IP='local_host_IP'
   export ROS_NAME='local_host_IP'

   source /opt/ros/kinetic/setup.bash
   source $HOME/catkin_ws/devel/setup.bash
   ```

   As you can see, `ROS_MASTER_URI` corresponds to the IP address of the vehicle and in `local_host_IP` you must enter the IP address of your personal computer.

`Remember: Your computer and the vehicles' computers must be connected on the same network` 

Launch ROS navigation stack
===========================

Physically place the SDV at home (ask for instructions to the LabFabEx technical staff).

Open a terminal via SSH. On the vehicle prompt `sdvunx@sdvunx-desktop:~$ ` type the following command:

```bash
roslaunch agv_nav agv_nav.launch
``` 
If this is the first time you run rviz on your computer, run the following command that will allow you to load the appropriate rviz configuration to navigate with the mobile robots:

```bash
scp ssh sdvunx@192.168.1.xx:/home/sdvunx/.rviz/default.rviz ~/.rviz/
```

This command launch the ROS navigation stack

On your own prompt type the following command:

```bash
rviz
``` 
`Nota temporal: Después de inicializar rviz mover manualmente el robot 10cm hacia adelante sin alzarlo (rodadandolo lentamente)`

This command launch the 3D visualization tool for ROS that allows to instuct the goals to acomplish by the vehicle and visualize the vehicle's navigation status. 

| ![rviz.png](img/rviz.png) |
|:--:|
| *As an example, this is a screenshot of rviz* |

Turn Off vehicles
=================

When the job is finished, you must turn off the vehicle. For this, execute the following command:

On the vehicle prompt `agvunx@agvunx-desktop:~$ ` type the following command:

```bash
sudo poweroff
``` 

Wait about 10 seconds until the vehicle's computer shuts down and then turn off all the switches placed on the back of the vehicle.

`It is IMPERATIVE to disconnect all batteries and their respective buzzers before returning the vehicle to LabFabEx technical staff.`

**If during operation any buzzer is activated please turn off the computer vehicle immediately and change the batteries.** 
